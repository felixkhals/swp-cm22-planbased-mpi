#!/bin/bash

# the export not done directly since not all shells allow the value of a
# variable to be set at the same time it is exported
OMPI=/home/openmpi-install; export OMPI
DPM_AGENT_PORT=25000; export DPM_AGENT_PORT
# BEWARE: the order (custom OpenMPI first/prepended) is important since the
# first gets picked and there may be other MPI installations on the system!
PATH=$OMPI/bin:$PATH; export PATH
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$OMPI/bin; export LD_LIBRARY_PATH

PROCESSES=1

HOSTFILE="--hostfile hostfile.txt"
# makes the usage of hardware-threads instead of just cores possible
ENABLE_HYPERTHREADING="--bind-to hwthread"
ALLOW_OVERLOAD="--bind-to core:overload-allowed"
ENABLE_OVERSUBSCRIBE="--map-by :OVERSUBSCRIBE"
#FIXME run as root is necessary for running it inside docker but it should be disabled for production!
ALLOW_RUN_AS_ROOT="--allow-run-as-root"
mpirun --np $PROCESSES $ALLOW_OVERLOAD $ENABLE_OVERSUBSCRIBE $HOSTFILE $ALLOW_RUN_AS_ROOT hello
