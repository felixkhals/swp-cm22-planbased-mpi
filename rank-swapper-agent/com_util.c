/* Quelle: http://www.netzmafia.de/skripten/inetprog/ThomasSocket2.pdf */

/* ------------------------------------------------------------------------------ */
/* C-Quell-Modul com_util.c                                                       */
/* ------------------------------------------------------------------------------ */
/* Hilfsfunktionen, die in den Programmen für locserv und loccli                  */
/* eingesetzt werden                                                              */
/* ------------------------------------------------------------------------------ */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

/* ------------------------------------------------------------------------------ */
/* Ausgabe einer Meldung msg, gefolgt von der Fehlermeldung, die dem in errno     */
/* gespeicherten Fehlercode entspricht                                            */
/* anschließende Beendigung des Programms                                         */

void errorExit(char* msg) {
    perror(msg);
    exit(1);
}

