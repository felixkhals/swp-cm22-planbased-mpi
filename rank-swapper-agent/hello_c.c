/*
 * Copyright (c) 2004-2006 The Trustees of Indiana University and Indiana
 *                         University Research and Technology
 *                         Corporation.  All rights reserved.
 * Copyright (c) 2006      Cisco Systems, Inc.  All rights reserved.
 *
 * Sample MPI "hello world" application in C
 */

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/un.h>

#include "mpi.h"

#define BUFFLEN 64

/* Spawn Modes (dynamic job spawning):
 *   1: Spawn just one process (in one job)
 *   2: Spawn 2 processes in 2 different jobs
 *   3: Spawn 2 prcoesses in one (shared) job
 *   0 or other: Do not spawn a dynamic process/job
 */
#define SPAWN_MODE 0

void errorExit(char* msg);

/*void notifyProcessAgent(pid_t pid, int rank, const char *eventInfo) {
    struct sockaddr_un strAddr;
    socklen_t lenAddr;
    int fdSock;
    
    if ((fdSock=socket(PF_UNIX, SOCK_STREAM, 0)) < 0) {
        errorExit("socket");
    }
    
    strAddr.sun_family=AF_LOCAL;     // Unix domain
    strcpy(strAddr.sun_path, SOCKET_PATH);
    lenAddr=sizeof(strAddr.sun_family)+strlen(strAddr.sun_path);
    if (connect(fdSock, (struct sockaddr*)&strAddr, lenAddr) !=0 ) {
        errorExit("connect");
    }
    
    char info2Send[BUFFLEN];
    snprintf(info2Send, BUFFLEN+1, "%s: %d, %d", eventInfo, pid, rank);
    if (send(fdSock, info2Send, BUFFLEN+1, 0) < 0) {
            errorExit("send");
    }
    printf("\nData send!\n");
    
    char rank2Recv[BUFFLEN];
    recv(fdSock, rank2Recv, BUFFLEN+1, 0);
    int receivedNumber = (int)strtol(rank2Recv, NULL, 0);
    printf("Received from server: %d\n", receivedNumber);
    
    close(fdSock);
}*/

int main(int argc, char* argv[]) {

    int rank, size, len;
    pid_t pid;
    char version[MPI_MAX_LIBRARY_VERSION_STRING];

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    pid = getpid();
    
    //notifyProcessAgent(pid, rank, "Spawned");
    
    printf("Hello, world, I am %d of %d, PID: %d\n", rank, size, pid);
    
    // dynamically spawn child process
    // https://mpi.deino.net/mpi_functions/MPI_Comm_spawn.html
    if (1 == SPAWN_MODE) {
    
        int np = 1;
        int errcodes[1];
        MPI_Comm parentcomm, intercomm;
        MPI_Comm_get_parent( &parentcomm );
        if (parentcomm == MPI_COMM_NULL) {
            MPI_Comm_spawn( "hello", MPI_ARGV_NULL, np, MPI_INFO_NULL, 0,
                            MPI_COMM_WORLD, &intercomm, errcodes );
            printf("I'm the parent.\n");
        } else {
            printf("I'm the spawned.\n");
        }
        if (0 != errcodes[0]) {
            printf("ERROR_SPAWN: code: %d\n", errcodes[0]);
        }
        
    // (instead of Comm_multiple) spawns a second, different intercommunicator
    } else if (2 == SPAWN_MODE) {
    
        int np = 1;
        int errcodes[1];
        MPI_Comm parentcomm, intercomm;
        MPI_Comm_get_parent( &parentcomm );
        if (parentcomm == MPI_COMM_NULL) {
            for (int i = 0; i < 2; i++) {
                MPI_Comm_spawn( "hello", MPI_ARGV_NULL, np, MPI_INFO_NULL, 0,
                                MPI_COMM_WORLD, &intercomm, errcodes );
                if (0 != errcodes[0]) {
                    printf("ERROR_SPAWN: code: %d\n", errcodes[0]);
                }
            }
            printf("I'm the parent.\n");
        } else {
            printf("I'm the spawned.\n");
        }
        
    } else if (3 == SPAWN_MODE) {
    
        int np[2] = { 1, 1 };
        int errcodes[2];
        MPI_Comm parentcomm, intercomm;
        char *cmds[2] = { "hello", "hello" };
        MPI_Info infos[2] = { MPI_INFO_NULL, MPI_INFO_NULL };
        MPI_Comm_get_parent( &parentcomm );
        if (parentcomm == MPI_COMM_NULL) {
            // Create n more processes using the "hello" executable
             MPI_Comm_spawn_multiple(2, cmds, MPI_ARGVS_NULL, np, infos, 0, MPI_COMM_WORLD, &intercomm, errcodes );
            printf("I'm the parent.\n");
        } else {
            printf("I'm the spawned.\n");
        }
        for (int i = 0; i < 2; i++) {
            if (0 != errcodes[i]) {
                printf("ERROR_SPAWN: code: %d\n", errcodes[i]);
            }
        }
    }
    
    sleep(5);
    
    MPI_Finalize();
    
    //notifyProcessAgent(pid, rank, "Ended");

    return 0;
}

